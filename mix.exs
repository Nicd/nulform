defmodule Nulform.Mixfile do
    use Mix.Project

    def project do
        [ app: :nulform,
            version: "0.0.1",
            deps: deps ]
    end

    # Configuration for the OTP application
    def application do
        [
            #mod: {Nulform.run, []}
        ]
    end

    # Returns the list of dependencies in the format:
    # { :foobar, "0.1", git: "https://github.com/elixir-lang/foobar.git" }
    defp deps do
        [
            {:json, github: "cblage/elixir-json"},
            {:excoder, "1.0.0", git: "https://Nicd@bitbucket.org/Nicd/excoder.git"}
        ]
    end
end
