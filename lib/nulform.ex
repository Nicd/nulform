defmodule Nulform do

    def run() do
        {:ok, buffer} = :gen_server.start_link Nulform.Buffer, nil, []

        {:ok, connection} = :gen_server.start_link Nulform.Connection,
                            [
                                self(), buffer, 0, "irc.quakenet.org", 6667,
                                "Nulform", nil, "nulform", "␀form"
                            ], []
        buffer <- {:nulform, :buffer, :reset_connection, connection}
        :gen_server.cast connection, :connect

        #{:ok, ircnet_buffer} = :gen_server.start_link Nulform.Buffer, nil, []

        #{:ok, ircnet_connection} = :gen_server.start_link Nulform.Connection,
        #                    [
        #                        self(), ircnet_buffer, 1, "irc.cc.tut.fi", 6667,
        #                        "Nulform", nil, "nulform", "␀form"
        #                    ], []
        #ircnet_buffer <- {:nulform, :buffer, :reset_connection, ircnet_connection}
        #:gen_server.cast ircnet_connection, :connect

        {:ok, urlanalyzer} = :gen_server.start_link Nulform.Plugins.URLAnalyzer,
                            nil, []

        listen_loop urlanalyzer
    end

    def listen_loop(urlanalyzer) do
        receive do
            {:nulform, :msg_in, msg} ->
                IO.puts(to_binary(msg.connection_id) <> " -> " 
                    <> Nulform.Utilities.to_utf8 msg.raw_msg)
                :gen_server.cast urlanalyzer, msg

                case String.split msg.raw_msg do
                    [_, "376" | _] ->
                        :gen_server.cast msg.buffer, "JOIN #nulform"
                    _ ->
                end

        end
        listen_loop urlanalyzer
    end

end
