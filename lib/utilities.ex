defmodule Nulform.Utilities do
    @moduledoc """
    This module contains utilities for the bot that don't really go anywhere
    else.
    """

    @doc """
    Convert a string to UTF-8, trying valiantly to maintain the original
    content. There may be no way to really know the encoding of the input, so we
    will first try it as UTF-8 and then as latin-1 if it was not valid.
    Unfortunately there is *no* feasible way to guess between latin-1 and all of
    the other 8-bit encodings if we don't resort to smelly heuristics, so
    latin-1 is all we will support.

    Don't you just love character encodings?
    """
    def to_utf8(string) do
        if String.valid? string do
            string
        else
            latin1_to_utf8 string
        end
    end

    @doc """
    Convert a latin-1 string to UTF-8. Will give the wrong results if the
    original string is not actually latin-1.
    """
    def latin1_to_utf8(string) do
        :unicode.characters_to_binary string, :latin1, :utf8
    end


    @doc """
    Convert a number of bytes into a human readable binary.
    """
    def human_bytes(size) do
        human_bytes size, 1000
    end

    def human_bytes(size, factor) do
        human_bytes size, factor, 2
    end

    def human_bytes(size, factor, decimals) do
        human_bytes size, factor, decimals, ["B", "kB", "MB", "GB", "TP", "PB"]
    end

    def human_bytes(size, factor, decimals, [ext | rest]) when size >= factor do
        human_bytes size / factor, factor, decimals, rest
    end

    def human_bytes(size, factor, decimals, [ext | rest]) do
        float_to_binary(:erlang.float(size), [decimals: decimals]) <> " " <> ext
    end
end
