defmodule Nulform.Handler do
    @moduledoc """
        A handler manages plugins for a connection or other IRC message source.
        It takes in messages from the IRC server and relays them to plugins. It
        also starts up the plugins and manages their lifetime.
    """

    def run() do
        
    end
end
